import sys

input_file_name = 'input.txt'
output_file_name = 'output.txt'
try:
	result = ''
	file = open(input_file_name)
	lines = file.readlines()
	for line in lines:
		if line[0] == '<':
			line = line.replace('<', '** ')
			line = line.replace('>', '')
			print(line)
		else:
			if line != '\n':
				line = line.replace('\n', '')
				line = line.replace(': ', '|')
				line = '|' + line + '|'
				print(line)

		result += line + '\n'
	file = open(output_file_name, 'w')
	file.write(result)
except Exception as e:
	print('error')
finally:
	file.close()